Rails.application.routes.draw do
  root "static_pages#home"
  
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :email_confirmations, only: %w[edit]
  resources :password_resets,     only: %w[new create edit update]
  resources :posts,               only: %w[create destroy]
  resources :relationships,       only: %w[create destroy]
  
  get     "help"    ,to: "static_pages#help"
  get     "about"   ,to: "static_pages#about"
  get     "contact" ,to: "static_pages#contact"
  get     "signup"  ,to: "users#new"
  get     "signin"  ,to: "sessions#new"
  post    "signin"  ,to: "sessions#create"
  delete  "signout" ,to: "sessions#destroy"
end
