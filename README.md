# Ruby on Rails Tutorial: microblog application

This is a microblog application for the
[*Twitter like Microblog application*](https://aaron-microblog.herokuapp.com/)
based on the [Ruby on Rails Tutorial](https://www.learnenough.com/ruby-on-rails-6th-edition-tutorial) course by [Michael Hartl](https://www.michaelhartl.com/)