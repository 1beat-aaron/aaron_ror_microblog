require "test_helper"

class UsersSigninTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:JamesSmith)
  end

  test "successful sign in" do
    get signin_path
    post signin_path, params: { session: { email: @user.email, password: "123456" } }
    assert is_signed_in?
    assert_redirected_to @user
    follow_redirect!
    assert_template "users/show"
    assert_select "a[href=?]", signin_path,       count: 0
    assert_select "a[href=?]", user_path(@user),  count: 5
end

  test "sign in without an email" do
    get signin_path
    assert_template "sessions/new"
    post signin_path, params: { session: { email: "", password: "123456" } }
    assert_template "sessions/new"
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end

  test "sign in without a password" do
    get signin_path
    assert_template "sessions/new"
    post signin_path, params: { session: { email: "without.password@test.mail", password: "" } }
    assert_template "sessions/new"
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end

  test "sign in with remembering" do
    signin_as(@user, remember_me: "1")
    assert_not_nil cookies[:remember_token]
  end

  test "sign in without remembering" do
    signin_as(@user, remember_me: "0")
    assert_nil cookies[:remember_token]
  end
end
