require "test_helper"

class UsersSignoutTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:JamesSmith)
  end

  test "successful sign in and then signed out" do
    get signin_path
    post signin_path, params: { session: { email: @user.email, password: "123456" } }
    assert is_signed_in?
    assert_redirected_to @user
    follow_redirect!
    assert_template "users/show"
    assert_select "a[href=?]", signin_path,       count: 0
    assert_select "a[href=?]", user_path(@user),  count: 5
    delete signout_path
    assert_not is_signed_in?
    assert_redirected_to root_url
    delete signout_path
    follow_redirect!
    assert_select "a[href=?]", signin_path,       count: 2
    assert_select "a[href=?]", user_path(@user),  count: 0
  end
end
