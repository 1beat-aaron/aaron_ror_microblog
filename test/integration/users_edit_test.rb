require "test_helper"

class UsersEditTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:JamesSmith)
    signin_as(@user)
  end

  test "successful user edit" do
    updated_nickname = "#{@user.nickname}_updated"
    updated_full_name = "#{@user.full_name}_updated"
    get edit_user_path(@user)
    patch user_path(@user), params: { user: {
                              nickname: updated_nickname,
                              full_name: updated_full_name,
                              email: @user.email
                            } }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal @user.nickname, updated_nickname
    assert_equal @user.full_name, updated_full_name
  end

  test "fail on update user with empty full name" do
    get edit_user_path(@user)
    patch user_path(@user), params: { user: { full_name: "", nickname: @user.nickname, email: @user.email } }
    assert_template "users/edit"
  end

  test "fail on update user with white spaces full name" do
    get edit_user_path(@user)
    patch user_path(@user), params: { user: { full_name: "             ", nickname: @user.nickname, email: @user.email } }
    assert_template "users/edit"
  end

  test "fail on update user with empty nickname" do
    get edit_user_path(@user)
    patch user_path(@user), params: { user: { full_name: @user.full_name, nickname: "", email: @user.email } }
    assert_template "users/edit"
  end

  test "fail on update user with white spaces nickname" do
    get edit_user_path(@user)
    patch user_path(@user), params: { user: { full_name: @user.full_name, nickname: "             ", email: @user.email } }
    assert_template "users/edit"
  end
end
