require "test_helper"

class PostsInterfaceTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:MichaelSmith)
  end

  test "post interface" do
    signin_as(@user)
    get root_path
    assert_select "ul.pagination", count: 2
    @user.posts.paginate(page: 1).each do |post|
      assert_match post.content, response.body
    end
    # Invalid submission
    assert_no_difference "Post.count" do
      post posts_path, params: { post: { content: "   " } }
    end
    assert_select "div.alert.alert-danger"
    # Valid submission
    post_content = "Valid submission"
    assert_difference "Post.count", 1 do
      post posts_path, params: { post: { content: post_content } }
    end
    assert_redirected_to root_path
    follow_redirect!
    assert_match post_content, response.body
    # Delete a post
    assert_select "a", text: "delete"
    first_post = @user.posts.paginate(page: 1).first
    assert_difference "Post.count", -1 do
      delete post_path first_post
    end
    # Visit a different user
    get users_path(users(:JohnPaul))
    assert_select "a", text: "delete", count: 0
  end
end
