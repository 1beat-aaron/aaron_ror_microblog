require "test_helper"

class PasswordResetsTest < ActionDispatch::IntegrationTest

  def setup
    ActionMailer::Base.deliveries.clear
    @user = users(:JamesSmith)
  end

  test "password resets" do
    get new_password_reset_path
    assert_template "password_resets/new"
    # Invalid email
    post password_resets_path, params: { password_reset: { email: "" } }
    assert_not flash.empty?
    assert_template "password_resets/new"

    # Valid email
    post password_resets_path, params: { password_reset: { email: @user.email } }
    assert_not_equal @user.reset_digest, @user.reload.reset_digest
    assert_equal 1, ActionMailer::Base.deliveries.size
    assert_not flash.empty?
    assert_redirected_to root_path
    
    # Password reset form
    user = assigns(:user)

    # Wrong email
    get edit_password_reset_path(user.reset_token, email: "")
    assert_not flash.empty?
    assert_template "password_resets/new"
    
    # Unconfirmed user
    user.toggle!(:confirmed)
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_not flash.empty?
    assert_redirected_to root_path
    user.toggle!(:confirmed)

    # Correct email, wrong token
    get edit_password_reset_path("#{user.reset_token}broken", email: user.email)
    assert_not flash.empty?
    assert_redirected_to root_path

    # Correct URL
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_template "password_resets/edit"
    assert_select "input[name=email][type=hidden][value=?]", user.email

    # Invalid password/confirmation combination
    patch password_reset_path(user.reset_token), params: {
                    email: user.email,
                    user: { password: "werqewrr",
                            password_confirmation: "opiopipi" } }
    assert_select "div.alert.alert-danger"
                        
    # Blank password
    patch password_reset_path(user.reset_token), params: {
                    email: user.email,
                    user: { password: "  ",
                            password_confirmation: "opiopipi" } }
    assert_not flash.empty?
    assert_template "password_resets/edit"

    # Blank confirmation
    patch password_reset_path(user.reset_token), params: {
                    email: user.email,
                    user: { password: "werqewrr",
                            password_confirmation: "  " } }
    assert_not flash.empty?
    assert_template "password_resets/edit"

    # Valid password/confirmation combination
    patch password_reset_path(user.reset_token), params: {
                    email: user.email,
                    user: { password: "werqewrr",
                            password_confirmation: "werqewrr" } }
    assert is_signed_in?
    assert_not flash.empty?
    assert_redirected_to user
  end
end
