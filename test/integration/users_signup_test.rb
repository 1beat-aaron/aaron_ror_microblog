require "test_helper"

class UsersSignupTest < ActionDispatch::IntegrationTest

  def setup
    ActionMailer::Base.deliveries.clear
  end

  test "successful signup" do
    get signup_path
    assert_difference "User.count", 1 do
      post users_path, params: { user: {  full_name:              "Successful Signup",
                                          nickname:               "successful_signup",
                                          email:                  "successful.signup@test.mail",
                                          password:               "123456",
                                          password_confirmation:  "123456" } }
    end
    assert_equal 1, ActionMailer::Base.deliveries.size
    user = assigns(:user)
    assert_not user.confirmed?
    # Try to sign in before email confirmation.
    signin_as user
    assert_not is_signed_in?
    # Try to confirm email using invalid confirmation token
    get edit_email_confirmation_path("invalid confirmation token")
    assert_not is_signed_in?
    # Try to confirm email using invalid email and valid confirmation token
    get edit_email_confirmation_path(user.confirmation_token, email: "wrong@test.mail")
    assert_not is_signed_in?
    # Confirm email using correct data
    get edit_email_confirmation_path(user.confirmation_token, email: user.email)
    assert user.reload.confirmed?
    follow_redirect!
    assert_template "users/show"
    assert is_signed_in?
  end

  test "signup with empty full name" do
    get signup_path
    assert_no_difference "User.count" do
      post users_path, params: { user: {  full_name:              "",
                                          nickname:               "empty_full_name",
                                          email:                  "empty.full.name@test.mail",
                                          password:               "123456",
                                          password_confirmation:  "123456" } }
    end
    assert_template "users/new"
  end

  test "signup with white spaces full name" do
    get signup_path
    assert_no_difference "User.count" do
      post users_path, params: { user: {  full_name:              "             ",
                                          nickname:               "white_space_full_name",
                                          email:                  "white.space.full.name@test.mail",
                                          password:               "123456",
                                          password_confirmation:  "123456" } }
    end
    assert_template "users/new"
  end

  test "signup with empty nickname" do
    get signup_path
    assert_no_difference "User.count" do
      post users_path, params: { user: {  full_name:              "Empty Nickname",
                                          nickname:               "",
                                          email:                  "empty.nickname@test.mail",
                                          password:               "123456",
                                          password_confirmation:  "123456" } }
    end
    assert_template "users/new"
  end

  test "signup with white spaces nickname" do
    get signup_path
    assert_no_difference "User.count" do
      post users_path, params: { user: {  full_name:              "White Space Nickname",
                                          nickname:               "             ",
                                          email:                  "white.space.nickname@test.mail",
                                          password:               "123456",
                                          password_confirmation:  "123456" } }
    end
    assert_template "users/new"
  end

  test "signup with empty email" do
    get signup_path
    assert_no_difference "User.count" do
      post users_path, params: { user: {  full_name:              "Empty Email",
                                          nickname:               "empty_email",
                                          email:                  "",
                                          password:               "123456",
                                          password_confirmation:  "123456" } }
    end
    assert_template "users/new"
  end

  test "signup with white spaces email" do
    get signup_path
    assert_no_difference "User.count" do
      post users_path, params: { user: {  full_name:              "White Space Email",
                                          nickname:               "white_space_email",
                                          email:                  "             ",
                                          password:               "123456",
                                          password_confirmation:  "123456" } }
    end
    assert_template "users/new"
  end

  test "signup with invalid email" do
    get signup_path
    assert_no_difference "User.count" do
      post users_path, params: { user: {  full_name:              "Invalid Email",
                                          nickname:               "invalid_email",
                                          email:                  "invalid.email@test,mail",
                                          password:               "123456",
                                          password_confirmation:  "123456" } }
    end
    assert_template "users/new"
  end

  test "signup with empty password" do
    get signup_path
    assert_no_difference "User.count" do
      post users_path, params: { user: {  full_name:              "Empty Password",
                                          nickname:               "empty_password",
                                          email:                  "empty.password@test.mail",
                                          password:               "",
                                          password_confirmation:  "123456" } }
    end
    assert_template "users/new"
  end

  test "signup with empty confirmation password" do
    get signup_path
    assert_no_difference "User.count" do
      post users_path, params: { user: {  full_name:              "Empty Confirmation Password",
                                          nickname:               "empty_confirmation_password",
                                          email:                  "empty.confirmation.password@test.mail",
                                          password:               "123456",
                                          password_confirmation:  "" } }
    end
    assert_template "users/new"
  end

  test "signup with confirmation password different from password" do
    get signup_path
    assert_no_difference "User.count" do
      post users_path, params: { user: {  full_name:              "Different Confirmation",
                                          nickname:               "different_confirmation",
                                          email:                  "different.confirmation@test.mail",
                                          password:               "123456",
                                          password_confirmation:  "987654" } }
    end
    assert_template "users/new"
  end
end
