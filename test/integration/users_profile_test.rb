require "test_helper"

class UsersProfileTest < ActionDispatch::IntegrationTest
  include ApplicationHelper

  def setup
    @user = users(:MichaelSmith)
  end

  test "profile display" do
    signin_as(@user)
    get user_path(@user)
    assert_template "users/show"
    assert_select "title", full_title(@user.full_name)
    assert_select "h4", text: @user.full_name
    assert_match @user.posts.count.to_s, response.body
    assert_select "ul.pagination", count: 2
    @user.posts.paginate(page: 1).each do |post|
      assert_match post.content, response.body
    end
  end
end
