require "test_helper"

class UsersIndexTest < ActionDispatch::IntegrationTest
  def setup
    @admin = users(:WilliamJeffrey)
    @non_admin = users(:JamesSmith)
  end

  test "index including pagination" do
    signin_as(@non_admin)
    get users_path
    assert_template "users/index"
    assert_select "ul.pagination", count: 2
    User.paginate(page: 1) do |user|
      assert_select "a[href=?]", user_path(user), text: user.full_name
    end
  end

  test "index as admin including pagination and delete buttons" do
    signin_as(@admin)
    get users_path
    assert_template "users/index"
    assert_select "ul.pagination", count: 2
    User.paginate(page: 1) do |user|
      assert_select "a[href=?]", user_path(user), text: user.full_name
      assert_select "button[data-action=?]",user_path(user), text: "Delete"
    end
    assert_difference "User.count", -1 do
      delete user_path(@non_admin), params: { confirm_delete_user: "delete" }
    end
  end

  test "index as non-admin including pagination" do
    signin_as(@non_admin)
    get users_path
    assert_template "users/index"
    assert_select "ul.pagination", count: 2
    User.paginate(page: 1) do |user|
      assert_select "a[href=?]", user_path(user), text: user.full_name
    end
  end
end
