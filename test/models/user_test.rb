require "test_helper"

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(
      full_name: "Example User",
      nickname: "example_user",
      email: "user@example.com",
      password: "password",
      password_confirmation: "password"
    )
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "name should be present" do
    @user.full_name = nil;
    assert_not @user.valid?
  end

  test "email should be present" do
    @user.email = nil
    assert_not @user.valid?
  end

  test "name should not be white spaces" do
    @user.full_name = "       ";
    assert_not @user.valid?
  end

  test "email should not be white spaces" do
    @user.email = "      "
    assert_not @user.valid?
  end

  test "name should not be longer than 71 include spaces" do
    @user.full_name = ("a" * 72)
    assert_not @user.valid?
  end

  test "email should not be longer than 255 include spaces" do
    @user.email = ("a" * 256)
    assert_not @user.valid?
  end

  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org first.last@foo.jp alice+bob@baz.cn first.last@1example.com]
    valid_addresses.each do |valid|
      @user.email = valid
      assert @user.valid?, "Email #{valid.inspect} should be valid"
    end
  end

  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example. foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid|
      @user.email = invalid
      assert_not @user.valid?, "Email #{invalid.inspect} should be invalid"
    end
  end

  test "email should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end

  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end

  test "authenticated? should return fasle for a user with nil remember_digest" do
    assert_not @user.authenticated?(:remember, "")
  end

  test "associated poosts should be destroyed" do
    @user.save
    @user.posts.create!(content: "Hello, World!")
    assert_difference "Post.count", -1 do
      @user.destroy
    end
  end

  test "should follow and unfollow a user" do
    david = users(:DavidRyan)
    richard = users(:RichardEric)

    assert_not david.following?(richard)
    david.follow(richard)
    assert david.following?(richard)
    assert richard.followers.include?(david)
    david.unfollow(richard)
    assert_not david.following?(richard)
  end

  test "feed should have the right posts" do
    follower = users(:JohnPaul)
    unfollowed = users(:DavidRyan)
    followed = users(:MichaelSmith)

    follower.follow(followed)
    # followed.follow(follower)

    # Posts from followed user
    followed.posts.each do |post_following|
      assert follower.feed.include?(post_following)
    end
    # Posts from self
    follower.posts.each do |post_self|
      assert follower.feed.include?(post_self)
    end
    # Posts from unfollowed user
    unfollowed.posts.each do |post_unfollowed|
      assert_not follower.feed.include?(post_unfollowed)
    end
  end
end
