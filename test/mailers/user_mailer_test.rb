require "test_helper"

class UserMailerTest < ActionMailer::TestCase
  
  def setup
    @user = users(:JamesSmith)
    @user.reset_token = User.token_base64
    @user.confirmation_token = User.token_base64
  end

  test "email_confirmation" do
    mail = UserMailer.email_confirmation(@user)
    assert_equal [@user.email], mail.to
    assert_equal ["noreply@example.com"], mail.from
    assert_equal "Microblog Email Confirmation", mail.subject
    
    assert_match "Hello", mail.body.encoded
    assert_match @user.full_name, mail.body.encoded
    assert_match @user.confirmation_token, mail.body.encoded
    assert_match CGI::escape(@user.email), mail.body.encoded
  end
  
  test "password_reset" do
    mail = UserMailer.password_reset(@user)
    assert_equal [@user.email], mail.to
    assert_equal ["noreply@example.com"], mail.from
    assert_equal "Microblog Password Reset", mail.subject

    assert_match "Hello", mail.body.encoded
    assert_match @user.full_name, mail.body.encoded
    assert_match @user.reset_token, mail.body.encoded
    assert_match CGI::escape(@user.email), mail.body.encoded
  end
end
