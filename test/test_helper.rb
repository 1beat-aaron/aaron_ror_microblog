ENV["RAILS_ENV"] ||= "test"
require_relative "../config/environment"
require "rails/test_help"
require "minitest/reporters"
Minitest::Reporters.use!

class ActiveSupport::TestCase
  # Run tests in parallel with specified workers
  parallelize(workers: :number_of_processors)

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Returns true if a test user signed in
  def is_signed_in?
    !session[:user_id].nil?
  end

  # Sign in as a particular user.
  def sign_in_as(user)
    session[:user_id] = user.id
  end
end

class ActionDispatch::IntegrationTest
  # Sign in as a particular user.
  def signin_as(user, password: "123456", remember_me: "1")
    post signin_path, params: { session: { email: user.email, password: password, remember_me: remember_me } }
  end
end
