require "test_helper"

class PostsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @post = posts(:hello_world)
  end

  test "should redirect create when not signed in" do
    assert_no_difference "Post.count" do
      post posts_path({ content: "Hello, World!" })
    end
    assert_redirected_to signin_path
  end

  test "should redirect destroy when not signed in" do
    assert_no_difference "Post.count" do
      delete post_path(@post)
    end
    assert_redirected_to signin_path
  end

  test "should redirect destroy for wrong post" do
    signin_as(users(:DavidRyan))
    assert_no_difference "Post.count" do
      delete post_path(@post)
    end
  end
end
