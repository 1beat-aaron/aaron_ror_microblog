require "test_helper"

class StaticPagesControllerTest < ActionDispatch::IntegrationTest

  test "should get home" do
    get root_url
    assert_response :success
    assert_select   "title",   "Microblog: Share your thoughts"
  end

  test "should get help" do
    get help_url
    assert_response :success
    assert_select   "title",   "Help | Microblog: Share your thoughts"
  end

  test "should get about" do
    get about_url
    assert_response :success
    assert_select   "title",  "About | Microblog: Share your thoughts"
  end

  test "should get contact" do
    get contact_url
    assert_response :success
    assert_select   "title",  "Contact | Microblog: Share your thoughts"
  end

  test "should get sign up" do
    get signup_url
    assert_response :success
    assert_select   "title",  "Sign up | Microblog: Share your thoughts"
  end
end
