require "test_helper"

class UsersControllerTest < ActionDispatch::IntegrationTest
  def setup
    @admin = users(:WilliamJeffrey)
    @non_admin = users(:JamesSmith)
  end

  test "should get new" do
    get signup_path
    assert_response :success
  end

  test "should redirect edit when not signed in" do
    get edit_user_path(@admin)
    assert_redirected_to signin_path
  end

  test "should redirect update when not signed in" do
    patch user_path(@admin),  params: { user: {
                              nickname: "#{@admin.nickname}_updated",
                              full_name: "#{@admin.full_name}_updated",
                              email: @admin.email
                            } }
    assert_redirected_to signin_path
  end

  test "should redirect edit when signed in another user" do
    signin_as(@non_admin)
    get edit_user_path(@admin)
    assert_not flash.empty?
    assert_redirected_to root_path
  end

  test "should redirect update when signed in another user" do
    signin_as(@non_admin)
    patch user_path(@admin),  params: { user: {
                              nickname: "#{@admin.nickname}_updated",
                              full_name: "#{@admin.full_name}_updated",
                              email: @admin.email
                            } }
    assert_not flash.empty?
    assert_redirected_to root_path
  end

  test "should redirect index when not signed in" do
    get users_path
    assert_redirected_to signin_path
  end

  test "should redirect destroy when not signed in" do
    assert_no_difference "User.count" do
      delete user_path(@non_admin)
    end
    assert_redirected_to signin_path
  end

  test "should redirect destroy when signed in as a non-admin user" do
    signin_as(@non_admin)
    assert_no_difference "User.count" do
      delete user_path(@admin)
    end
    assert_redirected_to root_path
  end

  test "should redirect following when not signed in" do
    get following_user_path @non_admin
    assert_redirected_to signin_path
  end

  test "should redirect followers when not signed in" do
    get followers_user_path @non_admin
    assert_redirected_to signin_path
  end
end
