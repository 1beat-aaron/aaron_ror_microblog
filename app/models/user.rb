class User < ApplicationRecord
    has_many :posts, dependent: :destroy
    has_many :active_relationships, class_name:     "Relationship",
                                    foreign_key:    "follower_id",
                                    dependent:      :destroy
    has_many :following, through: :active_relationships, source: :followed

    has_many :passive_relationships, class_name:     "Relationship",
                                     foreign_key:    "followed_id",
                                     dependent:      :destroy
    has_many :followers, through: :passive_relationships, source: :follower

    attr_accessor   :remember_token, :confirmation_token, :reset_token
    before_save     :downcase_email
    before_create   :create_confirmation_digest

    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z0-9\-.]+\.[a-z]+\z/i

    validates :full_name,   presence: true,
                            length: { maximum: 71 }
    validates :nickname,    presence: true,
                            length: { maximum: 50 },
                            uniqueness: true
    validates :email,       presence: true,
                            length: { maximum: 255 },
                            format: { with: VALID_EMAIL_REGEX },
                            uniqueness: true
    has_secure_password

    validates :password,    length: { minimum: 6 }  ,allow_nil: true

    # Returns the hash digest of the given unencrypted password
    def User.digest(unencrypted_password)
        cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
        BCrypt::Password.create(unencrypted_password, cost: cost)
    end

    # Returns URL safe base64 token
    def User.token_base64
        SecureRandom.urlsafe_base64
    end

    # Remembers a user in the database for use in presistent sessions.
    def remember
        self.remember_token = User.token_base64
        update_attribute(:remember_digest, User.digest(remember_token))
    end

    # Forgets a user.
    def forget
        update_attribute(:remember_digest, nil)
        self.remember_token = nil
    end

    # Returns true if the given token matches the digest.
    def authenticated?(attribute, token)
        digest = self.send("#{attribute}_digest")
        return false if digest.nil?
        BCrypt::Password.new(digest).is_password?(token)
    end

    # Send a confirmation email
    def send_confirmation_email
        UserMailer.email_confirmation(self).deliver
    end

    # Confirms the email a user signed up with.
    def confirm
        self.update_attribute(:confirmed, true)
        self.update_attribute(:confirmed_at, Time.zone.now)
    end

    # Sets the password reset attributes.
    def create_reset_digest
        self.reset_token = User.token_base64
        update_attribute(:reset_digest, User.digest(reset_token))
        update_attribute(:reset_sent_at, Time.zone.now)
    end

    # Sends password reset email.
    def send_password_reset_email
        UserMailer.password_reset(self).deliver
    end

    # Returns true if a password reset has expired.
    def password_reset_expired?
        reset_sent_at < 2.hours.ago
    end

    # Returns a user's feed
    def feed
        following_ids_sublelect = "SELECT followed_id FROM relationships WHERE follower_id = :user_id"
        Post.where("user_id IN (#{following_ids_sublelect}) OR user_id = :user_id", user_id: id)
    end

    def follow(user)
        following << user
    end

    def unfollow(user) 
        following.delete(user)
    end

    def following?(user)
        following.include?(user)
    end

    private
        # Converts email to all lower-case
        def downcase_email
            self.email = email.downcase
        end

        # Creates and assignes the confirmation token and digest.
        def create_confirmation_digest
            self.confirmation_token = User.token_base64
            self.confirmation_digest = User.digest(confirmation_token)
        end
end
