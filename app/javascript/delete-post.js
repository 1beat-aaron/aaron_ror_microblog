document.getElementById("deletePostModal")
    .addEventListener("show.bs.modal", (onDeletePostModalShow) => {
        const form = onDeletePostModalShow.target.getElementsByTagName("form")[0];

        form.action = onDeletePostModalShow.relatedTarget.dataset.action;

        // Remove form action attribute on modal close to prevent wrong post deletion
        onDeletePostModalShow.target
            .addEventListener("hide.bs.modal", (onDeletePostModalHide) => form.removeAttribute("action"), { once: true });
    });