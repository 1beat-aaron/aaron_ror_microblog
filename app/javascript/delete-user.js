document.getElementById("deleteUserModal")
    .addEventListener("show.bs.modal", (modalEvent) => {
        const form = modalEvent.target.getElementsByTagName("form")[0];

        const submitButton = Array.from(form.querySelectorAll("button"))
            .find((button) => button.type === "submit");

        const confirmationInput = Array.from(form.querySelectorAll("input"))
            .find((input) => input.id === "confirm-delete-user");

        const toggleSubmitButtonDisabled = (inputEvent) => {
            submitButton.disabled = inputEvent.target.value !== "delete";
        };

        confirmationInput.addEventListener("input", toggleSubmitButtonDisabled);

        form.action = modalEvent.relatedTarget.dataset.action;

        modalEvent.target.addEventListener("hide.bs.modal", (deleteUserModalHide) => {
            // Remove form action attribute on modal close to prevent wrong user deletion
            form.removeAttribute("action");
            confirmationInput.removeEventListener("input", toggleSubmitButtonDisabled);
        }, { once: true });
    });
