class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.email_confirmation.subject
  #
  def email_confirmation(user)
    @user = user
    @greeting = "Hello, #{@user.full_name}"
    mail(:to => @user.email, :subject => "Microblog Email Confirmation")
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def password_reset(user)
    @user = user
    @greeting = "Hello, #{@user.full_name}"
    mail(:to => @user.email, :subject => "Microblog Password Reset")
  end
end
