class PasswordResetsController < ApplicationController
  before_action :get_user,          only: %w[edit update]
  before_action :valid_user,        only: %w[edit update]
  before_action :check_expiration,  only: %w[edit update]

  def new
  end

  def create
    @user = User.find_by(email: params[:password_reset][:email].downcase)

    if !@user
      flash.now[:danger] = "Email address not found"
      render :new
    elsif !@user.confirmed?
      flash[:warning] = "Please confirm your email address first"
      redirect_to root_path
    else
      @user.create_reset_digest
      UserMailer.password_reset(@user).deliver
      flash[:info] = "Email sent with password reset instructions"
      redirect_to root_path
    end
  end

  def edit
  end

  def update
    if params[:user][:password].blank?
      flash[:danger] = "Password can't be blank."
      render "edit"
    elsif @user.update(user_params)
      sign_in(@user)
      flash[:success] = "Password has been reset"
      redirect_to @user
    else
      render "edit"
    end
  end

  private
    def user_params
      params.require(:user).permit(:password, :password_confirmation)
    end

    # Before filters

    def get_user
      @user = User.find_by(email: params[:email])
    end

    # Confirmes a valid user.
    def valid_user
      if !@user
        flash.now[:danger] = "Email address not found"
        render :new
      elsif !@user.confirmed?
        flash[:warning] = "Please confirm your email address first"
        redirect_to root_path
      elsif !@user.authenticated?(:reset, params[:id])
        flash[:danger] = "The link is invalid"
        redirect_to root_path
      end
    end

    # Checks expiration of reset token.
    def check_expiration
      if @user.password_reset_expired?
        flash[:danger] = "Password reset link has expired."
        redirect_to new_password_reset_path
      end
    end
end
