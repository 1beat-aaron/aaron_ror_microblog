class UsersController < ApplicationController
  before_action :authorized,  only: %w[index edit update destroy following followers]
  before_action :permitted,   only: %w[edit update]
  before_action :admin_user,  only: %w[destroy]

  def index
    @users = User.paginate(page: params[:page])
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    if @user.save
      UserMailer.email_confirmation(@user).deliver
      flash[:info] = "Please check your email #{@user.email} and follow confirmation instructions"
      redirect_to root_path
    else
      render :new, status: :bad_request
    end
  end

  def show
    @user = User.find(params[:id])
    @posts = @user.posts.paginate(page: params[:page])
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])

    if @user.update(user_params)
      flash[:success] = "Successfully updated"
      redirect_to @user
    else
      render :edit, status: :bad_request
    end
  end

  def destroy
    user = User.find_by(id: params[:id])

    if !user
      flash[:warning] = "User not found"
      status = :not_found
    elsif params[:confirm_delete_user] == "delete"
      user.destroy
      flash[:info] = "#{user.full_name} deleted"
      status = :ok
    else
      flash[:danger] = "Failed deleting #{user.full_name}"
      status = :bad_request
    end

    redirect_to users_path
  end

  def following
    @title = "Following"
    @user = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render "show_follow"
  end
  
  def followers
    @title = "Followers"
    @user = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render "show_follow"
  end

  private
    def user_params
      params.require(:user).permit(:full_name, :nickname, :email, :password, :password_confirmation)
    end

    # Users before filters

    # Confirms the user has permissions
    def permitted
      unless current_user.id.to_s == params[:id].to_s
        flash[:danger] = "Access denied"
        redirect_to root_path
      end
    end

    # Confirms an admin user has permissions
    def admin_user
      unless current_user.admin?
        flash[:danger] = "Access denied"
        redirect_to root_path
      end
    end
  end
