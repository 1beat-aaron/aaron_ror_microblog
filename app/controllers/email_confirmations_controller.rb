class EmailConfirmationsController < ApplicationController
    def edit
        user = User.find_by(email: params[:email])

        if !!(user && !user.confirmed? && user.authenticated?(:confirmation, params[:id]))
            user.confirm
            sign_in user
            flash[:success] = "Email confirmed!"
            redirect_to user
        else
            flash[:danger] = "Invalid activation link"
            redirect_to root_path
        end
    end
end
