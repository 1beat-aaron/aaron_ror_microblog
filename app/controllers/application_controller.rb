class ApplicationController < ActionController::Base
    include SessionsHelper

    # Common before filters

    # Confirms an authorized user
    def authorized
        unless signed_in?
            store_location
            flash[:danger] = "Please sign in"
            redirect_to signin_path
        end
    end
end
