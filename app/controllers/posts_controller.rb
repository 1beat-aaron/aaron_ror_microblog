class PostsController < ApplicationController
    before_action :authorized,  only: %w[create destroy]
    before_action :get_post,    only: %w[destroy]

    def create
        @post = current_user.posts.build(post_params)

        if @post.save
            redirect_to root_path
        else
            @feed_items = []
            render "static_pages/home"
        end
    end

    def destroy
        @post.destroy
        flash[:success] = "Post deleted"
        redirect_to root_path
    end

    private
        def post_params
            params.require(:post).permit(:content, :image)
        end

        def get_post
            @post = current_user.posts.find_by(id: params[:id])

            if @post.nil?
                flash[:warning] = "Post not found"
                redirect_to root_path
            end
        end
end
