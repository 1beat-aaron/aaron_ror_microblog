class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email])

    if !!(user && user.authenticate(params[:session][:password]))
      if user.confirmed?
        sign_in user
        params[:session][:remember_me] == "1" ? remember(user) : forget(user)
        flash[:success] = "Hello, #{user.full_name}! Welcome to Microblog!"
        redirect_to(session.delete(:return_to) || user)
      else
        flash[:warning] = "Email not confirmed. Check your email for the confirmation instructions"
        redirect_to root_path
      end
    else
      flash.now[:danger] = "Invalid email/password combination"
      render "new", status: :bad_request
    end
  end

  def destroy
    sign_out if signed_in?
    redirect_to root_url
  end
end
