class RelationshipsController < ApplicationController
    before_action :authorized,  only: %w[create destroy]

    def create
        user = User.find_by(id: params[:followed_id ])
        if !user
            flash[:danger] = "User not found"
            redirect_to root_path
        else
            current_user.follow(user)
            redirect_to user
        end
    end

    def destroy
        relationship = Relationship.find_by(id: params[:id])

        if !relationship
            flash[:danger] = "Association not found"
            redirect_to root_path
        else
            user = relationship.followed
            current_user.unfollow(user)
            redirect_to user
        end
    end
end
